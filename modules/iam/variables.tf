variable "project_id" {
  description = "The id for the project."
  type        = string
}

variable "org_id" {
  description = "The organization ID."
  type        = string
}