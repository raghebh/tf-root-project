output "bucket" {
  description = "The bucket name"
  value       = google_storage_bucket.state_bucket.name
}