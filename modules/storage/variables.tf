variable "project_id" {
  description = "The id for the project."
  type        = string
}