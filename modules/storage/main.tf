resource "google_storage_bucket" "state_bucket" {
  name          = "tf-states-bucket"
  location      = "US"
  force_destroy = true
  project       = var.project_id
}