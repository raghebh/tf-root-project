output "cluster_endpoint" {
  description = "The IP address of the cluster master."
  value       = google_container_cluster.gke.endpoint
}

output "cluster_name" {
  description = "The cluster name."
  value       = google_container_cluster.gke.name
}