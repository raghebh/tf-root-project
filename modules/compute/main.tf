resource "google_service_account" "gke_sa" {
  account_id   = "gke-sa"
  display_name = "GKE Service Account"
  project      = var.project
}

resource "google_container_cluster" "gke" {
  name                     = "${var.namespace}-k8s-cluster"
  location                 = var.zone
  project                  = var.project
  network                  = var.vpc.self_link
  remove_default_node_pool = true
  initial_node_count       = 1
}

resource "google_container_node_pool" "primary_preemptible_nodes" {
  name       = "${var.namespace}-k8s-pool"
  location   = var.zone
  project    = var.project
  cluster    = google_container_cluster.gke.name
  node_count = var.node_count

  node_config {
    preemptible     = true
    machine_type    = "e2-medium"
    service_account = google_service_account.gke_sa.email
    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]
  }
}