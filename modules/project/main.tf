locals {
  parent = "organizations/${var.org_id}"
}

module "project-factory" {
  source          = "terraform-google-modules/project-factory/google//modules/fabric-project"
  version         = "~> 10.1"
  name            = var.name
  prefix          = var.prefix
  parent          = local.parent
  billing_account = var.billing_account
  owners          = var.owners
  viewers         = var.viewers
  activate_apis   = var.activate_apis
}
