output "project_id" {
  description = "The created project ID."
  value       = module.project.project_id
}

output "bucket" {
  description = "The bucket name"
  value       = module.storage.bucket
}

output "cluster_name" {
  description = "The cluster name."
  value       = module.compute.cluster_name
}