module "iam" {
  source = "./modules/iam"

  org_id     = var.org_id
  project_id = module.project.project_id
}

module "storage" {
  source = "./modules/storage"

  project_id = module.project.project_id
}

module "compute" {
  source = "./modules/compute"

  namespace = var.namespace
  project   = module.project.project_id

  vpc = module.network.vpc
}

module "network" {
  source = "./modules/network"

  namespace = var.namespace
  project   = module.project.project_id
}

module "project" {
  source = "./modules/project"

  name            = var.project_name
  prefix          = var.prefix
  org_id          = var.org_id
  billing_account = var.billing_account
  owners          = var.owners
  viewers         = var.viewers
  activate_apis   = var.activate_apis
}