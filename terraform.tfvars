project_name = "root-pj"
prefix       = "tf"
owners       = ["user:zandolsi@acegik.fr"]
activate_apis = [
  "cloudbilling.googleapis.com",
  "serviceusage.googleapis.com",
  "cloudresourcemanager.googleapis.com",
  "container.googleapis.com",
  "servicenetworking.googleapis.com",
  "sqladmin.googleapis.com",
]
namespace = "transverse"
